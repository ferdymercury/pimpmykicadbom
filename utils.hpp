#ifndef UTILS_HPP_INCLUDED
#define UTILS_HPP_INCLUDED

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <string_view>
#include <algorithm>
#include <filesystem>
#include <cmath>

inline static void str_lower(std::string& s)
{
    std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c){ return std::tolower(c); });
}

template <typename T>
bool get_element_name(const T *e, std::string& s) /// true on success
{
    assert(e != nullptr);
    auto res = e->Name();
    if (res != nullptr) { s = res; return true; }
    s = "";
    return false;
}
template <typename T>
bool get_element_value(const T *e, std::string& s) /// true on success
{
    assert(e != nullptr);
    auto res = e->Value();
    if (res != nullptr) { s = res; return true; }
    s = "";
    return false;
}
template <typename T>
bool get_element_text(const T *e, std::string& s) /// true on success
{
    assert(e != nullptr);
    auto res = e->GetText();
    if (res != nullptr) { s = res; return true; }
    s = "";
    return false;
}

inline static bool str_rem_newlines(std::string& s) /// true if modified
{
    std::size_t pos0 = 0;
    std::size_t pos1 = 0; // = str.find("\n");
    std::string res;
    bool rrr = false;
    while (true)
    {
        pos1 = s.find('\n',pos0);
        std::string str1 = s.substr(pos0, (pos1-pos0));
        if (!str1.empty())
        {
            if (!res.empty()) { res += '_'; rrr = true; }
            res += str1;
        }
        if (pos1 == std::string::npos) { break; }
        pos0 = pos1+1;
    }
    //const bool rrr = (s.compare(res) == 0);
    s = res;
    return rrr;
}

inline static bool str_compare(const char* s0, const char* s1) /// true on match, case-sensitive
{
    if ((s0 == nullptr) || (s1 == nullptr))
    {
        return (s0 == s1);
    }
    //size_t i =
    while (true)
    {
        if (*s0 != *s1) { return false; }
        if (*s0 == '\0') { return true; }
        ++s0; ++s1;
    }
}
inline static bool str_compare(const std::string& s0, const std::string& s1) /// true on match, case-sensitive
{
    if (s0.length() != s1.length()) { return false; }
    return (s0.compare(s1) == 0);
}

inline static bool str_compare_nocs(const char* s0, const char* s1) /// true on match, case-insensitive
{
    if ((s0 == nullptr) || (s1 == nullptr))
    {
        return (s0 == s1);
    }
    //size_t i =
    while (true)
    {
        if (*s0 != *s1)
        {
            if (std::tolower(*s0) != std::tolower(*s1)) { return false; }
            //return false;
        }
        if (*s0 == '\0') { return true; }

        ++s0; ++s1;
    }
}
inline static bool str_compare_nocs(const std::string& s0, const std::string& s1) /// true on match, case-insensitive
{
    if (s0.length() != s1.length()) { return false; }
    return str_compare_nocs(s0.c_str(), s1.c_str());
}

// ###################################################################
template<typename T = uint64_t>
struct fumber_t /// Fancy nUMBER ... because i didn't have a better name for this
{
    enum TYPE
    {
        T_DECIMAL,  //!< 123
        T_HEX,      //!< 0x0F2C
        T_BIN,      //!< #10010000
        T_BIN2,     //!< 0b10010000
        T_ERROR     //!< Som Ting Wong
    };
    TYPE type;
    size_t num_chars; // the 0x, #, and 0b are not counted
    T val;
    fumber_t() : type(T_ERROR), num_chars(1), val(0)
    {}

    bool good() const
    {
        // max num_chars worst case should be the number of bits
        return ((type >= T_DECIMAL) && (type < T_ERROR) && (num_chars <= (2*8*sizeof(T))));
    }

    bool set(const std::string& s)
    {
        type = T_ERROR;
        if (s.empty()) { return false; }
        val = 0;
        const size_t len = s.length();
        size_t i = 0;
        {
            // try decimal, if fail due to weird chars - try the other ways
            while (i < len)
            {
                char c = s[i];
                if ((c >= '0') && (c <= '9')) { val = 10*val+(c-'0'); }
                else { break; }
                ++i;
            }
            if (i == len)
            {
                num_chars = i;
                type = T_DECIMAL;
                return true;
            }
        }
        if ((s[0] == '#') && (len > 1)) // cmsis-svd kind of #binary
        {
            i = 1;
            while (i < len)
            {
                char c = s[i];
                if ((c == '0') || (c == '1')) { val = (val<<1) | (c-'0'); }
                else { return false; }
                ++i;
            }
            num_chars = len-1;
            type = T_BIN;
            return true;
        }
        else if ((s[0] == '0') && (len > 2))
        {
            i = 2;
            if (s[1] == 'b') // binary2
            {
                while (i < len)
                {
                    char c = s[i];
                    if ((c == '0') || (c == '1')) { val = (val<<1) | (c-'0'); }
                    else { return false; }
                    ++i;
                }
                num_chars = len-2;
                type = T_BIN2;
                return true;
            }
            else if (s[1] == 'x') // hex
            {
                while (i < len)
                {
                    char c = s[i];
                    if      ((c >= '0') && (c <= '9')) { c -= '0'; }
                    else if ((c >= 'A') && (c <= 'F')) { c = (10+c-'A'); }
                    else if ((c >= 'a') && (c <= 'f')) { c = (10+c-'a'); }
                    else { return false; }
                    val = (val<<4) | c;
                    ++i;
                }
                num_chars = len-2;
                type = T_HEX;
                return true;
            }
        }
        return false;
    }

    void operator= (const std::string& s) { set(s); }
    void operator= (const char* s) { set(s); }

    template<typename T2> void operator= (const T2& x) { val = x; }
    operator T() { return val; }

    std::string str() const
    {
        static constexpr char lut_bin_nib[] =
        {
            "0000" "1000" "0100" "1100" "0010" "1010" "0110" "1110"
            "0001" "1001" "0101" "1101" "0011" "1011" "0111" "1111"
        };
        std::string s = "#";
        switch (type)
        {
            case T_DECIMAL:
            {
                s = std::to_string(val);
                if (s.length() < num_chars) { std::string t(num_chars - s.length(), ' '); s = t + s; }
                return s;
            } break;
            case T_HEX:
            {
                // try to fit it into "num_chars"
                // if it's shorter - add zeros in front
                //size_t i = 0;
                //if (val == 0) { s = "0";}
                std::string t;
                T x = val;
                while (x)
                {
                    const uint8_t b = x&0xFF;
                    x = (x>>8);
                    const uint8_t b1 = (b >> 4);
                    const uint8_t b0 = (b&0x0F);
                    static constexpr char lut_hex[16] =
                    {
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
                    };
                    t += lut_hex[b0];
                    t += lut_hex[b1];
                }
                size_t n = t.length();
                size_t i = n;
                while (i < num_chars) { t += '0'; ++i; }
                i = 0; n = t.length();
                s = "0x";
                while (i < n) { ++i; s += t[n-i]; }
                return s;
            }
            case T_BIN2:
                s = "0b";
                [[fallthrough]];
            case T_BIN:
            {
                std::string t;
                T x = val;
                while (x)
                {
                    const uint8_t b = x&0xFF;
                    x = (x>>8);
                    const uint8_t b1 = (b >> 4)<<2;
                    const uint8_t b0 = (b&0x0F)<<2;
                    t += lut_bin_nib[b0];
                    t += lut_bin_nib[b0+1];
                    t += lut_bin_nib[b0+2];
                    t += lut_bin_nib[b0+3];
                    t += lut_bin_nib[b1];
                    t += lut_bin_nib[b1+1];
                    t += lut_bin_nib[b1+2];
                    t += lut_bin_nib[b1+3];
                }
                size_t n = t.length();
                size_t i = n;
                while (i < num_chars) { t += '0'; ++i; }
                i = 0; n = t.length();
                while (i < n) { ++i; s += t[n-i]; }
                return s;
            }
            case T_ERROR: break;
            // default: don't put a default!
        };
        s = "<error>";
        std::cerr << "* Error: bad fumber conversion!\n";
        throw std::logic_error("this fumber was bad, cannot convert it to string.");
        return s;
    }
};

struct z_logger
{
    bool open(const std::string& fn) { of.open(fn); return of.is_open(); }
    template<class... Ts> void operator() (const Ts& ... args)
    {
        (std::cout << ... << args);
        if (of.is_open())
        {
            (of << ... << args);
            //(std::cout << ... << args);
            if (of.tellp() > 1024*1024*4)
            {
                std::cerr << " * CLOSING LOG FILE * ";
                of.close();
            }
        }
    }

    std::ofstream of;
};

static z_logger logger;
using fumber64_t = fumber_t<uint64_t>;

template<typename T, std::ostream &os = std::cout>
void print_vec(const std::vector<T> &vec, const std::string name)
{
    os << name << "[] =\n{\n";
    std::size_t j = 0;
    for (const auto& i: vec)
    {
        os << "  [" << j << "] " << i << "\n"; ++j;
    }
    os << "}\n";
}

inline static void str_replace(std::string& subject, const std::string& search, const std::string& replace)
{
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos)
    {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
}

struct skip_first_delimiter
{
    skip_first_delimiter(const std::string _what) : once(true), what(_what) {}
    std::string operator()()
    {
        const bool x = once;
        once = false;
        if (x == false) { return what; }
        return std::string("");
    }
    bool once;
    std::string what;
};

inline static bool get_default_cfg_fn(std::string &fn, const std::vector<std::string> &v_altdir, const std::string what = "config file")
{
    // fn should contain a filename (like config.cfg)
    // this checks if it exists in any of the paths in v_altdir
    // return true on success and the result is in fn
    std::ostringstream os;
    namespace fs = std::filesystem;

    //for (const std::string &alt : v_altdir)
    // search backwards
    size_t i = v_altdir.size();
    while (i)
    {
        --i;
        const std::string &alt = v_altdir.at(i);
        fs::path p = fs::absolute(alt);
        p /= fn;
        std::cout << "- checking: " << p.string() << "\n";
        if (fs::exists(p)) { fn = p.string(); return true; }
        else { os << "Error: " << what << ' ' << p << " does not exist.\n"; }
    }
    std::cerr << os.str();
    return false;
}

/// Get a path suitable for storing configs for this user, the path might not exist! empty path on fail.
std::filesystem::path get_userconfig_path(const std::string appname = "");


struct crc32a
{
    // http://www.hackersdelight.org/hdcodetxt/crc.c.txt

    uint32_t crc;
    uint32_t reverse(uint32_t x)
    {
        x = ((x & 0x55555555) <<  1) | ((x >>  1) & 0x55555555);
        x = ((x & 0x33333333) <<  2) | ((x >>  2) & 0x33333333);
        x = ((x & 0x0F0F0F0F) <<  4) | ((x >>  4) & 0x0F0F0F0F);
        x = (x << 24) | ((x & 0xFF00) << 8) | ((x >> 8) & 0xFF00) | (x >> 24);
        return x;
    }
    void init()
    {
        crc = 0;
        crci = 0xFFFFFFFF;
        b = 0xFFFFFFFF;
    }
    uint32_t calc(uint32_t word, const unsigned nbits = 16)
    {
        b = reverse(word);
        unsigned j = 0;
        while (j < nbits) // 8 times for 8 bits? maybe it should be 16 for the xmega?
        {
            union
            {
                uint32_t ui;
                int32_t si;
            };
            ui = crci ^ b;
            if (si < 0)
            {
                crci = (crci << 1) ^ (0x04C11DB7);
            }
            else
            {
                crci = (crci << 1);
            }
            b = (b << 1); // next bit
            ++j;
        }
        return reverse(~crci);
    }
    void process(const uint8_t *p, size_t n)
    {
        size_t i = 0;
        while (i < n)
        {
            crc = calc(*p);
            ++p; ++i;
        }
    }
    private:
    uint32_t crci, b;
};

struct ghcl_t
{
    // ghetto HCL
    float hf(float _h)
    {
        _h -= std::floor(_h);
        _h *= 3.f;
        if      (_h >= 2.f) { _h = 0.f; }
        else if (_h >  1.f) { _h = (2.f - _h); }
        //_h = std::pow((_h + _h * (1.f-_h)), 1.414f);
        _h = std::pow(_h, (1.f - _h * _h));
        return _h;
    }
    bool calc_rgb(float &r, float &g, float &b)
    {
        if (l <= 0.f)
        {
            r = 0.f; g = 0.f; b = 0.f;
            if (l < 0.f) { return false; }
            return true;
        }
        if (c <= 0.f)
        {
            r = l;
            g = l;
            b = l;
            if (l >= 1.f) { return false; }
            if (c <  0.f) { return false; }
            return true;
        }
        r = hf(h + 0.f/3.f);
        g = hf(h + 1.f/3.f);
        b = hf(h + 2.f/3.f);
        //r = std::pow(r, 0.707); g = std::pow(g, 0.707); b = std::pow(b, 0.707);
        //constexpr float zdb = 0.367879441171f;
        //constexpr float zdb = 0.0722f;
        //r /= (0.2126f + zdb);
        //g /= (0.7152f + zdb);
        //b /= (0.0722f + zdb);
        float Y;
        //Y = (0.2126f * r) + (0.7152f * g) + (0.0722f * b);
        Y = hf(1.f/6.f) * 1.f;
        r = Y + c * (r-Y);
        g = Y + c * (g-Y);
        b = Y + c * (b-Y);
        Y = (0.2126f * r) + (0.7152f * g) + (0.0722f * b);
        float G = l/Y;
        r *= G;
        g *= G;
        b *= G;
        if (b > 1.f) { return false; }
        if (r > 1.f) { return false; }
        if (g > 1.f) { return false; }
        return true;
    }
    float h, c, l;
};

#endif // UTILS_HPP_INCLUDED
